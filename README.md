# libpaqle

-----------------------------------

A Java library to query Paqle. Work in progress.

[![Build Status](https://drone.data.coop/api/badges/nellemann/libpaqle/status.svg)](https://drone.data.coop/nellemann/libpaqle)

## Usage

```java
Paqle paqle = new Paqle("MyCompany", "myUsername", "myPassword");
PaqleResponse response = paqle.getResponse(["57020415"]);
```

### Gradle

    repositories {
		maven { url 'https://git.data.coop/api/packages/nellemann/maven' }
    }

    dependencies {
        compile 'biz.nellemann.libs:libpaqle:1.+'
    }

## Development

To build and test the code:

    ./gradle build
    ./gradle test
