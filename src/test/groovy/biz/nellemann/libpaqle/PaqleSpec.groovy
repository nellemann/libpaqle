package biz.nellemann.libpaqle

import biz.nellemann.libpaqle.pojo.PaqleResponse
import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import spock.lang.Specification

class PaqleSpec extends Specification {

    Paqle paqle
	MockWebServer mockServer = new MockWebServer();

    def setup() {
        paqle = new Paqle("TestCompany", "testUser", "testPassword")
		mockServer.start();
	}

    def cleanup() {
		mockServer.shutdown()
	}

	void "test successful parsing of test JSON"() {

		setup:
		def testFile = new File(getClass().getResource('/15027800.json').toURI())
		def testJson = testFile.getText('UTF-8')
		mockServer.enqueue(new MockResponse().setBody(testJson));
		HttpUrl baseUrl = mockServer.url("/");
		paqle.baseUrl = baseUrl.toString()

		when:
		PaqleResponse response = paqle.getResponseByVatNumber("15027800")

		then:
		response.entities.size() == 1
		response.news.size() == 100
		response.news[0].headline.text.contains('Opkøb skubber til Frejas vækst')
	}

	// TODO: Test error-cases, 401, 404, timeouts, empty responses, etc.

}
