package biz.nellemann.libpaqle.pojo;

import com.google.gson.annotations.SerializedName;

public class NewsExtract {

	@SerializedName(value = "text", alternate = {"Text"})
	String text;

	@SerializedName(value = "highlight", alternate = {"Highlight", "HighLight"})
	Boolean highlight;

}
