package biz.nellemann.libpaqle.pojo;

import com.google.gson.annotations.SerializedName;

public class NewsHeadline {

	@SerializedName(value = "text", alternate = {"Text"})
	String text;

}
