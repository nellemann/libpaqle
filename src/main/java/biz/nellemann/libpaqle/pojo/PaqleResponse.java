package biz.nellemann.libpaqle.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

// https://entityapi.paqle.net/documentation/entities

public class PaqleResponse {

	@SerializedName(value = "entities", alternate = {"Entities"})
	List<Entity> entities;

	@SerializedName(value = "news", alternate = {"News"})
	List<News> news;

}

