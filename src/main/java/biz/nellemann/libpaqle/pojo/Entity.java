/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package biz.nellemann.libpaqle.pojo;

import com.google.gson.annotations.SerializedName;

public class Entity {

	@SerializedName(value = "published", alternate = {"Published"})
	Boolean published;

	@SerializedName(value = "hidden", alternate = {"Hidden"})
	Boolean hidden;

	@SerializedName(value = "type", alternate = {"Type"})
	String type;

	@SerializedName(value = "countryCode", alternate = {"CountryCode"})
	String countryCode;

	@SerializedName(value = "paqleUrl", alternate = {"PaqleUrl"})
	String paqleUrl;

	/*
	"Entities": [
    {
      "Published": true,
      "SectionsDk": {
        "CvrMetadata": [
          {
            "CvrId": "4004110055"
          }
        ]
      },
      "MergedIds": [
        1818574
      ],
      "Hidden": false,
      "OriginalId": 1495393,
      "CurrentId": 7906322,
      "Type": "Person",
      "CountryCode": "dk",
      "PaqleUrl": "https://www.paqle.dk/p/lars-fruergaard-jørgensen/1495393"
    },

  ],*/
}
