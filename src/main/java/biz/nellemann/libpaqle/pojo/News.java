package biz.nellemann.libpaqle.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class News {

	@SerializedName(value = "entityOriginalId", alternate = {"EntityOriginalId"})
	Long entityOriginalId;

	@SerializedName(value = "clusterHash", alternate = {"ClusterHash"})
	String clusterHash;

	@SerializedName(value = "published", alternate = {"Published"})
	Date published;

	@SerializedName(value = "languageCode", alternate = {"LanguageCode"})
	String languageCode;

	@SerializedName(value = "sourceName", alternate = {"SourceName"})
	String sourceName;

	@SerializedName(value = "url", alternate = {"Url", "URL"})
	String url;

	@SerializedName(value = "headline", alternate = {"Headline", "HeadLine"})
	List<NewsHeadline> headline;

	@SerializedName(value = "extract", alternate = {"Extract"})
	List<NewsExtract> extract;

	/*
	News": [
    {
      "EntityOriginalId": 11801,
      "ClusterHash": "fbdcfe3faff89fd16e0172148a1503f8",
      "Published": "2019-04-02T19:58Z",
      "LanguageCode": "da",
      "SourceName": "Finans.dk",
      "Url": "http://entityapi.paqle.net/articles/2ad41f1fbcf45cf495cf5d2b089d81",
      "Headline": [
        {
          "Text": "Nationalbanken holder sig fra valutaindgreb trods svækkelse"
        }
      ],
      "Extract": [
        {
          "Text": "Hos "
        },
        {
          "Text": "Sydbank",
          "Highlight": true
        },
        {
          "Text": " ser man den manglende intervention som et tegn på, at Nationalbanken er \"komfortabel\" med situationen."
        }
      ]
    }
  ]*/
}
