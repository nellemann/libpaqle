/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package biz.nellemann.libpaqle;

import biz.nellemann.libpaqle.pojo.PaqleResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Paqle {

    private final static Logger log = LoggerFactory.getLogger(Paqle.class);
    private final OkHttpClient client;

    private final String company;
    private final String basicUsername;
    private final String basicPassword;

    protected String baseUrl = "https://entityapi.paqle.net";


    public Paqle(String company, String basicUsername, String basicPassword) {
        this.company = company;
        this.basicUsername = basicUsername;
		this.basicPassword = basicPassword;
        client = new OkHttpClient();
    }


    protected String get(String url) throws Exception {

        String credential = Credentials.basic(basicUsername, basicPassword);

        Request request = new Request.Builder().url(url)
                .header("Authorization", credential).addHeader("Accept", "application/json;").build();

        Response response = client.newCall(request).execute();
        switch (response.code()) {
	        case 200:
	            return response.body().string();
	        case 401:
	        	log.warn("get() - 401 - Access Denied");
	            throw new Exception("Access Denied");
			case 404:
				log.warn("get() - 404 - Not Found");
				throw new Exception("Not Found");
	        default:
	            throw new Exception("get() - Unknown Error - status code: " + response.code());
        }

    }


    protected String getJson(String vatNumber) throws Exception {
    	//String cvrIds =  String.join(",", vatNumbers);
        String jsonText = get(baseUrl + "/entities?customer=" + company + "&news&vatNumbers=" + "DK" + vatNumber);
        log.debug("getJson() response: " + jsonText);
        return jsonText;
    }


	protected PaqleResponse deserializeJson(String json) throws JsonSyntaxException {
		//Gson gson = new Gson();
		Gson gson = new GsonBuilder()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ssz")
			.create();
		PaqleResponse response = gson.fromJson(json, PaqleResponse.class);
		return response;
	}


    protected PaqleResponse getResponseByVatNumber(String vatNumber) {
		PaqleResponse response;
    	try {
    		String json = getJson(vatNumber);
    		response = deserializeJson(json);
		} catch ( Exception e) {
    		log.error("Error", e);
    		return null;
		}
		return response;
    }


}
